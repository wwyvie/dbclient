(ns dbclient.core
  (:require [dbclient.network :as network]
            [dbclient.config :as config]))

(defn run-thread [properties threadnum]
  (let [conn (network/connect (:host @properties)
                              (:port @properties))]
    (future (binding [*in* (:in conn)
                      *out* (:out conn)]
              (println (get @properties (keyword (str "query" threadnum))))
              (read-line)))))

(defn -main [& args]
  (do
    (config/load-properties)
    (let [thread-num (:thread-num (deref config/properties))
          port (:port (deref config/properties))]
      (if (and (integer? thread-num)
               (integer? port)
               (> thread-num 0)
               (> port 0))
        (doseq [x (for [i (range thread-num)]
                    [i (run-thread config/properties i)])]
          (println "Result for thread " (first x) " is " (deref (second x))))
        (println "Wrong parameters!")))))
