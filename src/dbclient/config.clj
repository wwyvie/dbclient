(ns dbclient.config
  (:import [java.util Properties]
           [java.io FileInputStream]))

(def file-config "dbclient.properties")

(def ^:private defaults {:host "localhost"
                         :port 9997
                         :query0 "select student"
                         :thread-num 1})

(def properties (atom defaults))

(defn ^:private parse-int [^String val]
  (try
    (Integer/parseInt val)
    (catch NumberFormatException e
      nil)))

(defn ^:private try-to-convert-to-int [^String val]
  (if-let [intval (parse-int val)]
    intval
    val))

(defn ^:private load-default-params []
  (reset! properties defaults))

(defn load-properties []
  (try (let [prop (Properties.)]
         (.load prop (FileInputStream. file-config))
         (doseq [[k v] prop]
           (swap! properties assoc (keyword k) (try-to-convert-to-int v)))
         properties)
       (catch Exception e
         properties)))
