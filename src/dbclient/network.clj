(ns dbclient.network
  (:import [java.net Socket])
  (:require [clojure.java.io :as io]))

(defn connect [host port]
  (try
    (let [socket (Socket. host port)]
      {:in (io/reader (.getInputStream socket))
       :out (io/writer (.getOutputStream socket))})
    (catch Exception e
      nil)))
